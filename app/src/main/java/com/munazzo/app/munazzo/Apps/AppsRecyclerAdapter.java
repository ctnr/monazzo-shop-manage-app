package com.munazzo.app.munazzo.Apps;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.munazzo.app.munazzo.MobilWebView;
import com.munazzo.app.munazzo.R;

import java.util.List;

/**
 * Created by admin on 2.03.2017.
 */


    public class AppsRecyclerAdapter extends RecyclerView.Adapter<AppsRecyclerAdapter.AppsViewHolder> {
        private List<AppsPOJO> movies;
        private int rowLayout;
        private Context context;




        @Override
        public AppsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view= LayoutInflater.from(parent.getContext()).inflate(rowLayout,parent,false);
            return new AppsViewHolder(view);
        }


        @Override
        public void onBindViewHolder(AppsViewHolder holder, int position) {

        }


        @Override
        public int getItemCount() {
            return movies.size();
        }

        public static class AppsViewHolder extends RecyclerView.ViewHolder {

            CardView appcardView;
            TextView appName;
            ImageView appLogo;
            ImageButton aciklama;
            Button installeren;


            public AppsViewHolder(View itemView) {
                super(itemView);
                appcardView=(CardView)itemView.findViewById(R.id.card_apps);
                appName=(TextView)itemView.findViewById(R.id.text_app_name);
                appLogo=(ImageView)itemView.findViewById(R.id.image_icon_app_item);
               // aciklama=(ImageButton) itemView.findViewById(R.id.imagebutton_app_item_detail);
                installeren=(Button)itemView.findViewById(R.id.button_app_install);

            }

        }
        public AppsRecyclerAdapter(List<AppsPOJO> movies, int rowLayout, Context context) {
            this.movies = movies;
            this.rowLayout = rowLayout;
            this.context = context;
        }


        public void onBindViewHolder(final AppsViewHolder holder, final int position, List<Object> payloads) {

            holder.appName.setText(movies.get(position).strTite);
            Glide.with(context).load(movies.get(position).strIcon).
                    thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL).
                    into(holder.appLogo);

            holder.installeren.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i=new Intent(context,MobilWebView.class);
                    i.putExtra("menu",7);
                    i.putExtra("link",movies.get(position).strLink);
                    context.startActivity(i);

                }
            });


        }

    }

