package com.munazzo.app.munazzo.Zoeken;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.munazzo.app.munazzo.EndlessRecyclerViewScrollListener;
import com.munazzo.app.munazzo.Login.sessionControl;
import com.munazzo.app.munazzo.R;
import com.munazzo.app.munazzo.RetroClient;
import com.munazzo.app.munazzo.RetroInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 29.12.2016.
 */

public class zoeken extends android.support.v4.app.Fragment {

    RecyclerView recyclerView;
    List<pojoClass> movieData;
    List<pojoClass> loadData;
    movieAdapter moviewAdapter,loadAdapter;
    SearchView searchView;
    sessionControl session;
    LinearLayoutManager linearLayoutManager;
    RetroInterface service;

    private EndlessRecyclerViewScrollListener scrollListener;
    AlertDialog progres;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.zoeken,container,false);
        progres=new SpotsDialog(getContext());


        session =new sessionControl(getContext());

        recyclerView=(RecyclerView)view.findViewById(R.id.main_recycler);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager=new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(linearLayoutManager);

        movieData=new ArrayList<>();

        searchView=(SearchView)view.findViewById(R.id.search);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                ara(query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });


        return view;
    }

    String klm;
    private void ara(String kelime){
        klm=kelime;
        progres.show();
        searchView.clearFocus();
        service= RetroClient.getClient(getContext()).create(RetroInterface.class);
        Call<pojoClass[]> call = service.getJsonValues(kelime);
        call.enqueue(new Callback<pojoClass[]>() {
        @Override
        public void onResponse(Call<pojoClass[]> call, Response<pojoClass[]> response) {

            movieData = Arrays.asList(response.body());
            moviewAdapter = new movieAdapter(movieData, R.layout.item_list,getActivity());
            recyclerView.setAdapter(moviewAdapter);
            progres.dismiss();
        }

        @Override
        public void onFailure(Call<pojoClass[]> call, Throwable t) {
            Log.e("arama kisminda  ", t.getMessage());
        }
    });



     scrollListener =new EndlessRecyclerViewScrollListener(linearLayoutManager) {
         @Override
         public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {



            Call<pojoClass[]> load=service.getJsonValuePage(klm, page + 1);
             load.enqueue(new Callback<pojoClass[]>() {
                 @Override
                 public void onResponse(Call<pojoClass[]> call, Response<pojoClass[]> response) {

                     final List<pojoClass>data= Arrays.asList(response.body());


                     recyclerView.post(new Runnable() {
                         @Override
                         public void run() {
                             moviewAdapter.setItem(data);
                             moviewAdapter.notifyItemRangeInserted(recyclerView.getAdapter().getItemCount()-1,data.size());
                         }
                     });

                 }

                 @Override
                 public void onFailure(Call<pojoClass[]> call, Throwable t) {
                        Log.e("Error: LoadMoreListener",t.getMessage());
                 }
             });
         }


     };
        recyclerView.addOnScrollListener(scrollListener);



        }



    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }



}
