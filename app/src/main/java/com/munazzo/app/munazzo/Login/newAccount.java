package com.munazzo.app.munazzo.Login;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.munazzo.app.munazzo.R;

/**
 * Created by admin on 21.02.2017.
 */

public class newAccount extends android.support.v4.app.Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.new_account,container,false);

        WebView webViewNewAccount=(WebView)view.findViewById(R.id.newAccount);
        WebSettings settings=webViewNewAccount.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        settings.setBuiltInZoomControls(false);
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        webViewNewAccount.setWebViewClient(new WebViewClient());
        webViewNewAccount.setWebChromeClient(new WebChromeClient());

        if (Build.VERSION.SDK_INT >= 19) {
            webViewNewAccount.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        else {
            webViewNewAccount.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        webViewNewAccount.loadUrl("https://munazzo.com/app/register?stealthmode=1");

        return view;
    }
}
