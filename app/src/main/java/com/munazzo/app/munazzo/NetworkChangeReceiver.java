package com.munazzo.app.munazzo;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import dmax.dialog.SpotsDialog;

/**
 * Created by admin on 11.04.2017.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {
    private static final String LOG_TAG = "internet Kontrol¸";
    static boolean isConnected = false;
    public AlertDialog progres;
    @Override
    public void onReceive(final Context context, final Intent intent) {
        if(progres == null) {
            progres = new SpotsDialog(context, "Er is geen verbinging",R.style.CustomMunazzo);
            progres.setCancelable(false);
        }
        if (!isNetworkAvailable(context)){
            progres.show();
        }else{
            if(progres.isShowing()) {
                progres.dismiss();
            }

        }
    }


    private boolean isNetworkAvailable(Context context) {


        ConnectivityManager connectivity = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE); //Sistem ağını dinliyor internet var mı yok mu

        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {

                        if(!isConnected){ //internet varsa
                            isConnected = true;
                            Log.v(LOG_TAG, "bagli!");
                            if(progres != null)
                            {
                                if(progres.isShowing()) {
                                    progres.dismiss();
                                }
                            }
                        }

                        return true;
                    }
                }
            }
        }
        isConnected = false;
        Log.v(LOG_TAG, "internet Yok!");
        return false;
    }
}
