package com.munazzo.app.munazzo.Details;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.munazzo.app.munazzo.R;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Created by admin on 14.02.2017.
 */

public class specificationDetail extends android.support.v4.app.Fragment {
    TableLayout tablo;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.detail_specificaties,container,false);
        String gelen=getArguments().getString("spec");
        JsonParser parser=new JsonParser();
        JsonArray specs= (JsonArray) parser.parse(gelen);
        String bslk,sl,sg;
        tablo=(TableLayout)view.findViewById(R.id.textDetail);
        for (int i=0; i<specs.size();i++){


            JsonObject ozellik= (JsonObject) specs.get(i);
            //yazdir = ozellik.get("strName").getAsString()+" \n";

            TableRow rowBaslik=new TableRow(getContext());
            rowBaslik.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
            TextView baslik=new TextView(getContext());
            baslik.setTextSize(16);
            baslik.setTextColor(Color.DKGRAY);
            baslik.setMaxWidth(rowBaslik.getLayoutParams().width);
            rowBaslik.setHorizontalGravity(Gravity.CENTER);
            rowBaslik.setPadding(0,10,0,10);

            bslk=ozellik.get("strName").getAsString();
            baslik.setText(bslk);
            rowBaslik.addView(baslik);
            tablo.addView(rowBaslik);
            JsonArray feature= ozellik.getAsJsonArray("arrFeatures");

            for(int j=0;j<feature.size();j++){
                JsonObject satir=(JsonObject)feature.get(j);
                //yazdir += satir.get("strName").getAsString()+"  :  "+satir.get("strValue").getAsString()+"\n";

                TableRow rowSatir=new TableRow(getContext());
                rowSatir.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.FILL_PARENT));
                TextView sol=new TextView(getContext());
                TextView sag=new TextView(getContext());

                sl=satir.get("strName").getAsString();
                sg=satir.get("strValue").getAsString();
                sol.setText(sl); sag.setText(sg); rowSatir.addView(sol); rowSatir.addView(sag);
                tablo.addView(rowSatir);


            }



        }
        return view;
    }
}
