package com.munazzo.app.munazzo.winkelwagen;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.munazzo.app.munazzo.Fragments.betalen;
import com.munazzo.app.munazzo.MobilWebView;
import com.munazzo.app.munazzo.R;
import com.munazzo.app.munazzo.RecyclerItemClickListener;
import com.munazzo.app.munazzo.RetroClient;
import com.munazzo.app.munazzo.RetroInterface;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 10.01.2017.
 */

public class winkelwagen extends android.support.v4.app.Fragment {
    TableLayout listViewSepet;
    int cartID;
    int userID;
    int productID;
    int quantity;
    String title;
    double price;

    Button betalen;
    TextView total,subtotal,btw,verzending;
    CartListPOJO data;
    ArrayList<CartListPOJO> dataList;
    RecyclerView recyclerCart;
    MyRecyclerViewAdapter myRecyclerViewAdapter;
    SpotsDialog progressDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.winkelwagen,container,false);

        progressDialog=new SpotsDialog(getContext(),R.style.CustomMunazzo);
        progressDialog.show();

        recyclerCart=(RecyclerView)view.findViewById(R.id.recycler_cart);
        recyclerCart.setHasFixedSize(true);
        recyclerCart.setLayoutManager(new LinearLayoutManager(getContext()));


        MobilWebView.mobilTitle.setText("Winkelwagen");

        btw=(TextView)view.findViewById(R.id.text_BTW);
        total=(TextView)view.findViewById(R.id.text_total);
        subtotal=(TextView)view.findViewById(R.id.text_subtotal);

        verzending=(TextView)view.findViewById(R.id.text_verzending);
        betalen=(Button)view.findViewById(R.id.button_betalen);

        sepet();





        ItemTouchHelper.SimpleCallback simpleCallback=new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int cartID=dataList.get(viewHolder.getAdapterPosition()).intCartID;
                int position=viewHolder.getAdapterPosition();
                delete(cartID,position);


            }

        };

        ItemTouchHelper iteItemTouchHelper=new ItemTouchHelper(simpleCallback);
        iteItemTouchHelper.attachToRecyclerView(recyclerCart);

        betalen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getFragmentManager().beginTransaction().replace(R.id.frame,new betalen()).commit();
            }
        });

        recyclerCart.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                edit(dataList.get(position).intCartID);

            }


        }));

        return view;
    }



    public void delete(int intCartid, final int position){

        RetroInterface service=RetroClient.getClient(getContext()).create(RetroInterface.class);
        Call<String> call =service.delete(intCartid);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Toast.makeText(getContext(), "Verwiyderd...", Toast.LENGTH_SHORT).show();

                sepet();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });




    }

    public void sepet(){

        RetroInterface service= RetroClient.getClient(getContext()).create(RetroInterface.class);
        Call<JsonObject> call=service.getcart();
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonArray responArray=response.body().getAsJsonArray("objCartCollection");
                dataList=new ArrayList<>();
                for (int i=0;i<responArray.size();i++){
                    data=new CartListPOJO();

                    JsonObject object=responArray.get(i).getAsJsonObject();

                    cartID=object.get("intCartID").getAsInt();
                    data.intCartID=cartID;
                    userID=object.get("intUserID").getAsInt();
                    data.intUserID=userID;
                    productID=object.get("intProductID").getAsInt();
                    data.intProductID=productID;
                    //productOfferID=object.get("intProductOffersID").getAsBigInteger();
                    //data.intProductOfferID=productOfferID;
                    quantity=object.get("intQuantity").getAsInt();
                    data.intQuantity=quantity;
                    title=object.get("strTitle").getAsString();
                    data.strTitle=title;
                    price=object.get("douPrice").getAsDouble();
                    data.duoPrice=price;
                    dataList.add(data);
                }

                myRecyclerViewAdapter=new MyRecyclerViewAdapter(dataList,R.layout.winkel_item,getContext());
                recyclerCart.setAdapter(myRecyclerViewAdapter);


                subtotal.setText(  "\t : €"+new DecimalFormat("####.##").format(response.body().get("subtotal").getAsDouble()));
                btw.setText(       "\t : €"+new DecimalFormat("####.##").format(response.body().get("vat").getAsDouble()));
                verzending.setText("\t : €"+new DecimalFormat("####.##").format(response.body().get("shipping").getAsDouble()));
                total.setText(     "\t : €"+new DecimalFormat("####.##").format(response.body().get("total").getAsDouble()));
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }
int miktar=0;
    public void edit(final int cartId){



        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("Edit");
        alertDialog.setMessage("Voer het aantal");


        final EditText input = new EditText(getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);

        alertDialog.setIcon(R.drawable.ic_edit);

        alertDialog.setPositiveButton("Ja",

                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        miktar = Integer.parseInt(String.valueOf(input.getText()));



                        RetroInterface editService=RetroClient.getClient(getContext()).create(RetroInterface.class);
                        Call<String> call =editService.editCartItem(cartId,miktar);
                        call.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                Log.e("import winkelwagen",response.body().toString());
                                sepet();
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {
                                Log.e("Error:winkelwagen ekle", t.getMessage());

                            }
                        });

                    }
                });

        alertDialog.setNegativeButton("Nee",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();

    }
}



