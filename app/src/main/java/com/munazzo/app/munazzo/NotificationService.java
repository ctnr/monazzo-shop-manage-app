package com.munazzo.app.munazzo;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by admin on 31.03.2017.
 */

public class NotificationService extends Service {

    protected void onCreate(Bundle savedInstanceState) {



    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("Start","Bildirim servisi başlatıldı");
        Timer timer = new Timer();
        timer.schedule( new TimerTask() {
            @Override
            public void run() {
                Intent resultent=new Intent(getBaseContext(),MainActivity.class);
                TaskStackBuilder taskStackBuilder= TaskStackBuilder.create(getBaseContext());
                taskStackBuilder.addParentStack(MainActivity.class);
                taskStackBuilder.addNextIntent(resultent);
                PendingIntent resultPending=taskStackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);


                NotificationCompat.Builder bildirim = new NotificationCompat.Builder(getBaseContext());
                bildirim.setSmallIcon(R.mipmap.ic_launcher);
                bildirim.setContentTitle("Yeni Bildirim");
                bildirim.setContentText("Merhaba Android Bildirim Merkezi");
                bildirim.setAutoCancel(true);
                bildirim.setTicker("buda bizim mesajimiz...");
                bildirim.setContentIntent(resultPending);
                NotificationManager bildirimYontetimi = (NotificationManager) getSystemService( Context.NOTIFICATION_SERVICE);
                bildirimYontetimi.notify(0, bildirim.build());
                Log.e("System Message","Bildirim gönder");


            }
        }, 0, 10000 );
        return START_STICKY;

    }

    @Override
    public void onDestroy() {

        Log.e("Destroy","Servis kapatıldı");

    }
}