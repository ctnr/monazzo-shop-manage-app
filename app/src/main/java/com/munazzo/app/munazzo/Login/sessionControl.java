package com.munazzo.app.munazzo.Login;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by admin on 7.02.2017.
 */

public class sessionControl {

    public Context mContext;
    public static SharedPreferences mPreferences;
    public SharedPreferences.Editor mEditor;

    public sessionControl(Context context){
        mContext=context;
        mPreferences=PreferenceManager.getDefaultSharedPreferences(mContext);

    }
    public void setmPreferences(boolean session, String cookie,String username,String password){
        mEditor=mPreferences.edit();
        mEditor.putBoolean("session",session);
        mEditor.putString("cookie",cookie);
        mEditor.putString("username",username);
        mEditor.putString("password",password);
        mEditor.commit();
    }
    public String getUsername(){
        return mPreferences.getString("username",null);
    }
    public String getPassword(){
        return mPreferences.getString("password",null);
    }
    public boolean getSession(){
        return mPreferences.getBoolean("session",false);
    }
    public String getCookie(){
        return mPreferences.getString("cookie","");
    }

}
