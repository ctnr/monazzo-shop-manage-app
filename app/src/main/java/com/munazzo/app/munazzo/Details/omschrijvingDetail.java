package com.munazzo.app.munazzo.Details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.munazzo.app.munazzo.R;

/**
 * Created by admin on 14.02.2017.
 */

public class omschrijvingDetail extends android.support.v4.app.Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.detail_omscrijving,container,false);

        String gelen=getArguments().getString("oms");

        TextView goster=(TextView)view.findViewById(R.id.textOmschrjv);
        goster.setText(Html.fromHtml(gelen));


        return view;
}
}
