package com.munazzo.app.munazzo.Bestellingen;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.munazzo.app.munazzo.R;

import java.util.List;

/**
 * Created by admin on 31.03.2017.
 */

public class bestellingenAdapter extends RecyclerView.Adapter<bestellingenAdapter.moviewViewHolder> {
    private List<bestellingenPOJO> movies;
    private int rowLayout;
    private Context context;




    @Override
    public bestellingenAdapter.moviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(rowLayout,parent,false);
        return new bestellingenAdapter.moviewViewHolder(view);
    }


    @Override
    public void onBindViewHolder(bestellingenAdapter.moviewViewHolder holder, int position) {

    }


    @Override
    public int getItemCount() {
        return movies.size();
    }

    public static class moviewViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView winkel,winkelID,verzonden,betaalen,datum,prijs;


        public moviewViewHolder(View itemView) {
            super(itemView);
            cardView=(CardView)itemView.findViewById(R.id.card_bestellingen);
            winkel=(TextView)itemView.findViewById(R.id.textView_bestellingen_winkel);
            winkelID=(TextView)itemView.findViewById(R.id.textView_bestellingen_winkelID);
            verzonden=(TextView)itemView.findViewById(R.id.textView_bestellingen_verzonden);
            betaalen=(TextView)itemView.findViewById(R.id.textView_bestellingen_betalen);
            datum=(TextView)itemView.findViewById(R.id.textView_bestellingen_datum);
            prijs=(TextView)itemView.findViewById(R.id.textView_bestellingen_prijs);
        }

    }
    public bestellingenAdapter(List<bestellingenPOJO> movies, int rowLayout, Context context) {
        this.movies = movies;
        this.rowLayout = rowLayout;
        this.context = context;
    }


    public void onBindViewHolder(final bestellingenAdapter.moviewViewHolder holder, final int position, List<Object> payloads) {

        bestellingenPOJO data=movies.get(position);
        holder.winkel.setText(data.winkel);
        holder.winkelID.setText(String.valueOf(data.winkelID));
        holder.datum.setText(data.datum);
        holder.prijs.setText(String.valueOf(data.prijs)+"€");

        if (movies.get(position).status!=4) {
            if (data.betaalen == 0) {
                holder.betaalen.setText("Niet Betaald");
                holder.betaalen.setBackgroundResource(R.color.munazzoFalse);
            } else {
                holder.betaalen.setText("Betalen");
                holder.betaalen.setBackgroundResource(R.color.munazzoTrue);
            }

            if (data.verzonden == 0) {
                holder.verzonden.setText("Niet Verzonden");
                holder.verzonden.setBackgroundResource(R.color.munazzoFalse);
            } else {
                holder.verzonden.setText("Verzonden");
                holder.verzonden.setBackgroundResource(R.color.munazzoTrue);
            }
        }else {
            holder.betaalen.setText("Geannuleerd");
            holder.betaalen.setBackgroundResource(R.color.munazzoBackround);
            holder.verzonden.setText("");
        }

    }

}

