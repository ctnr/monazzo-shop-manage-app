package com.munazzo.app.munazzo.Details;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.munazzo.app.munazzo.R;
import com.munazzo.app.munazzo.RetroClient;
import com.munazzo.app.munazzo.RetroInterface;

import java.text.DecimalFormat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 14.02.2017.
 */

public class informatieDetail extends android.support.v4.app.Fragment {


    String kisaAciklama;
    int stoc,deliv,productId,aant;
    double prij;
    EditText aantal;
    ImageView checkBoxVoorraad;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_informatie,container,false);


        stoc=getArguments().getInt("stock");
        deliv=getArguments().getInt("kargo");
        kisaAciklama=getArguments().getString("inf");
        productId=getArguments().getInt("intProductOfferId");
        prij=getArguments().getDouble("duoPrijs");



        final TextView sortDesc=(TextView)view.findViewById(R.id.textSortDesc);
        TextView delivery=(TextView)view.findViewById(R.id.textDelivery);
        TextView stock=(TextView)view.findViewById(R.id.textStockInf);
        TextView prijs=(TextView)view.findViewById(R.id.textPriceInf);
        Button addshopcart=(Button)view.findViewById(R.id.btnInWinkelwagen);
        aantal=(EditText)view.findViewById(R.id.textAantal);
        checkBoxVoorraad=(ImageView) view.findViewById(R.id.image_voorraad);



        sortDesc.setText(kisaAciklama);
        delivery.setText("\t Levertijd:\t\t\t"+deliv+" dag");
        if(stoc>0) {
            checkBoxVoorraad.setImageResource(R.drawable.okay_son);
            stock.setText("\t Voorraad:\t\t\t" + stoc + " stuk");
            stock.setTextColor(getResources().getColor(R.color.munazzoButton));
        }else{

            stock.setText("\t Voorraad:\t\t\t 0 stuk");
            stock.setTextColor(Color.BLACK);
        }
        prijs.setText("€ "+new DecimalFormat("####.##").format(prij));

        addshopcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (aantal.getText().length()>1) {
                    aant = Integer.parseInt(aantal.getText().toString());
                    aantal.setText("");
                    RetroInterface service = RetroClient.getStringClient(getContext()).create(RetroInterface.class);
                    Call<String> call = service.addtocart(productId, aant);
                    call.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            Log.e("Succes winkelwagen add",response.body().toString());
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {

                            Log.e("Error", t.getMessage());
                        }


                    });

                }
                else{
                    Toast.makeText(getContext(), R.string.bir_sayi_girmelisin , Toast.LENGTH_SHORT).show();
                }
            }
        });



        return view;
    }




}
