package com.munazzo.app.munazzo.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.munazzo.app.munazzo.Dashboard.dashboard;
import com.munazzo.app.munazzo.MainActivity;
import com.munazzo.app.munazzo.R;

/**
 * Created by admin on 30.03.2017.
 */

public class addNewApp extends Fragment {
    TextView note;
    Button app,terug;
    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.add_new_app,container,false);
        note=(TextView)view.findViewById(R.id.text_add_new_note);

        note.setText("Om de product import tool te kunnen gebruiken moet er een webshop integratie en/of marktplaatsen worden aangesloten.");

        app=(Button)view.findViewById(R.id.button_add_app);
        terug=(Button)view.findViewById(R.id.button_terug_app);
        terug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.frame,new dashboard(),null).commit();
            }
        });


        app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MainActivity.class);
                intent.putExtra("tab",2);
                startActivity(intent);
            }
        });



        return view;
    }

}

