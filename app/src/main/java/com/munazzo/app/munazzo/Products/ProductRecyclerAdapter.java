package com.munazzo.app.munazzo.Products;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.munazzo.app.munazzo.BusStation;
import com.munazzo.app.munazzo.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 13.03.2017.
 */

public class ProductRecyclerAdapter extends RecyclerView.Adapter<ProductRecyclerAdapter.moviewViewHolder> {
    private List<ProductPOJO> movies;
    private int rowLayout;
    private Context context;
    ArrayList<ProductPOJO> secilenData=new ArrayList<>();


    @Override
    public ProductRecyclerAdapter.moviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(rowLayout,parent,false);
        return new ProductRecyclerAdapter.moviewViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ProductRecyclerAdapter.moviewViewHolder holder, int position) {

    }


    @Override
    public int getItemCount() {
        return movies.size();
    }

    public static class moviewViewHolder extends RecyclerView.ViewHolder{

        CardView cardView;
        CheckBox checkBox;
        TextView Title,prijs;
        Spinner category;
        EditText marge,verkoop;


        public moviewViewHolder(View itemView) {
            super(itemView);
            cardView=(CardView)itemView.findViewById(R.id.card_product);

            checkBox= (CheckBox) itemView.findViewById(R.id.product_check);
            Title=(TextView)itemView.findViewById(R.id.product_product);

            category=(Spinner)itemView.findViewById(R.id.product_categorie);

            prijs=(TextView)itemView.findViewById(R.id.product_inkoop);
            marge=(EditText)itemView.findViewById(R.id.text_product_marge);
            verkoop=(EditText)itemView.findViewById(R.id.text_product_verkoop);

        }

    }
    public ProductRecyclerAdapter(List<ProductPOJO> movies, int rowLayout, Context context) {
        this.movies = movies;
        this.rowLayout = rowLayout;
        this.context = context;

    }


    public void onBindViewHolder(final ProductRecyclerAdapter.moviewViewHolder holder,final int position, List<Object> payloads) {


        ArrayAdapter adapter=new ArrayAdapter(context,android.R.layout.simple_spinner_dropdown_item, movies.get(position).categorys);
        holder.category.setAdapter(adapter);
        holder.category.setSelection(0);
        holder.Title.setText(movies.get(position).Title);
        holder.prijs.setText(new DecimalFormat("#####.##").format(movies.get(position).Prijs));
        holder.marge.setText(String.valueOf(movies.get(position).marge));
        holder.verkoop.setText(String.valueOf(movies.get(position).verkoop));
        holder.marge.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                    movies.get(position).marge = s.length()==0 ? 0: Integer.parseInt(s.toString());
                    movies.get(position).verkoop = (float) ((movies.get(position).marge * movies.get(position).Prijs) / 100 + movies.get(position).Prijs);
                    holder.verkoop.setText(new DecimalFormat("#####.##").format(movies.get(position).verkoop));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked) {
                   if (holder.category.getSelectedItemPosition()==0&&!movies.get(position).categorys.get(0).equals("Geen categorie..")){
                       Toast.makeText(context, "Je moet selectieren een categorie..", Toast.LENGTH_SHORT).show();
                       holder.checkBox.setChecked(false);
                   }
                    else{
                       movies.get(position).islem=true;
                       movies.get(position).selectCategoryID=movies.get(position).categoryID.get(holder.category.getSelectedItemPosition());
                      // movies.get(position).verkoop= Float.parseFloat(String.valueOf(holder.verkoop.getText()));
                   }
                }
                else{
                   movies.get(position).islem=false;
                }

            }
        });

        product.importeren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String toplam="";
                int i=0;
                for (ProductPOJO data:movies) {

                    if (data.islem) {
                        if (i!=0){
                            toplam+="|";

                        }
                        toplam += data.ProductID+":"+data.marge+":"+data.selectCategoryID+":"+data.verkoop;
                        i++;
                    }

                }

                BusStation.getBus().post(new Messenger(toplam));

            }
        });
    }

}
