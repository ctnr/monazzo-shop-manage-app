package com.munazzo.app.munazzo.Account;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.munazzo.app.munazzo.MobilWebView;
import com.munazzo.app.munazzo.R;
import com.munazzo.app.munazzo.RetroClient;
import com.munazzo.app.munazzo.RetroInterface;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by admin on 29.12.2016.
 */

public class account extends Fragment {

    TextView company,name,mail,adres,postcode,plaats,land;
    String sirket,isim,mailadres,adr,pstcode,plts,lnd,pakket;
    List<horizontalMenuPOJO> DataListAccont;
    RecyclerView recyclerAccount;
    AccountAdapter adapter;
    ImageView paketLogo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.account,container,false);

        MobilWebView.mobilTitle.setText("Account");

        paketLogo=(ImageView)view.findViewById(R.id.ImageView_account);
        company=(TextView)view.findViewById(R.id.text_company_name);
        name=(TextView)view.findViewById(R.id.text_account_name);
        mail=(TextView)view.findViewById(R.id.text_company_mail);
        adres=(TextView)view.findViewById(R.id.text_account_adres);
        postcode=(TextView)view.findViewById(R.id.text_account_postcode);
        plaats=(TextView)view.findViewById(R.id.text_account_plaats);
        land=(TextView)view.findViewById(R.id.text_account_land);


        recyclerAccount=(RecyclerView)view.findViewById(R.id.recycler_account_app);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerAccount.setLayoutManager(layoutManager);


        RetroInterface service= RetroClient.getClient(getContext()).create(RetroInterface.class);
        final Call<JsonObject> getAccount=service.getAccount();
        getAccount.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JsonObject mainObject=response.body();
                mailadres=mainObject.get("strEmail").getAsString();
                pakket=mainObject.get("objSubscription").getAsString();

                switch (pakket){
                    case "Royal": paketLogo.setImageResource(R.drawable.royal);  break;
                    case "Enterprise": paketLogo.setImageResource(R.drawable.enterprise);  break;
                    case "Basis": paketLogo.setImageResource(R.drawable.basis);  break;
                    case "Membership": paketLogo.setImageResource(R.drawable.membership);  break;

                }

                JsonObject objectCompany=mainObject.get("objCompany").getAsJsonObject();
                JsonObject objProperty=objectCompany.get("arrProperties").getAsJsonObject();
                isim=objProperty.get("strOwner").getAsString();
                sirket=objProperty.get("strName").getAsString();
                adr=objProperty.get("strStreet").getAsString();
                pstcode=objProperty.get("strZipcode").getAsString();
                plts=objProperty.get("strCity").getAsString();
                lnd=objProperty.get("strCountry").getAsString();

                company.setText(sirket);
                name.setText(isim);
                mail.setText(mailadres);
                adres.setText("\t : "+adr);
                postcode.setText("\t : "+pstcode);
                plaats.setText("\t : "+plts);
                land.setText("\t : "+lnd);

                DataListAccont=new ArrayList<horizontalMenuPOJO>();

                JsonArray platforms=mainObject.get("objPlatforms").getAsJsonArray();
                for(int i=0;i<platforms.size();i++){
                    JsonObject property= platforms.get(i).getAsJsonObject();

                    horizontalMenuPOJO data=new horizontalMenuPOJO();

                    JsonObject altProperty=property.get("arrProperties").getAsJsonObject();

                    data.appname=altProperty.get("strTitle").getAsString();
                    data.applogo="https://munazzo.com"+altProperty.get("strIcon").getAsString();

                    DataListAccont.add(data);


                }

                adapter=new AccountAdapter(DataListAccont, R.layout.account_item,getContext());
                recyclerAccount.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e("Error: Account",t.getMessage());
            }
        });





        return view;
    }

}
