package com.munazzo.app.munazzo.Details;

import android.app.AlertDialog;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TextView;

import com.munazzo.app.munazzo.R;
import com.munazzo.app.munazzo.RetroClient;
import com.munazzo.app.munazzo.RetroInterface;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class detailActivity extends AppCompatActivity  {

    String strTitle;
    int intProductID,intProductOfferId,intStock,kargo;
    double prijs;
    ViewPager viewPager;
    CostumPagerAdapter mCostumPagerAdapter;
    public JsonArray resimlerJson;
    private AlertDialog progressDialog;
    TableLayout tablo;
    String specString,omschString,infoString;
    TabLayout tabDetail;
    ArrayList<String > git;
    JsonObject xml;
    TextView titleDetail;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        kargo=getIntent().getIntExtra("ortalama",0);
        intStock=getIntent().getIntExtra("intStock",0);
        intProductID=getIntent().getIntExtra("intProductId",0);
        strTitle=getIntent().getStringExtra("strTitle");
        intProductOfferId=getIntent().getIntExtra("intProductOfferId",0);
        prijs=getIntent().getDoubleExtra("duoPrijs",0.0d);


        titleDetail=(TextView)findViewById(R.id.title_detail);

        if (strTitle.length()<26) {
            titleDetail.setText(strTitle);

        }else{
            titleDetail.setText(strTitle);
            //getSupportActionBar().setSubtitle(strTitle.substring(26, strTitle.length()));
        }
        tabDetail=(TabLayout)findViewById(R.id.tabsDetail);
        tabDetail.addTab(tabDetail.newTab().setText("Informatie"));
        tabDetail.addTab(tabDetail.newTab().setText("Omschrijving"));
        tabDetail.addTab(tabDetail.newTab().setText("Specification"));

        progressDialog=new SpotsDialog(this,R.style.CustomMunazzo);
        progressDialog.show();

        tablo=(TableLayout)findViewById(R.id.textDetail);



            RetroInterface service = RetroClient.getClient(getApplicationContext()).create(RetroInterface.class);
            Call<JsonObject> callDetail = service.getDetail(intProductID);
            callDetail.enqueue(new Callback<JsonObject>() {

                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        xml = response.body().get("arrXml").getAsJsonObject();
                        Doldur(xml);



                    } catch (Exception e) {

                    }


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });



        tabDetail.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                informatieDetail infomatie=new informatieDetail();
                omschrijvingDetail omschrijving=new omschrijvingDetail();
                specificationDetail specification=new specificationDetail();
                Bundle data=new Bundle();

                switch (tab.getPosition()){

                    case 0:
                        data.putString("inf",infoString);
                        data.putInt("stock",intStock);
                        data.putInt("kargo",kargo);
                        data.putInt("intProductId",intProductID);
                        data.putDouble("duoPrijs",prijs);
                        infomatie.setArguments(data);
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameDetail, infomatie).commit();
                        break;
                    case 1:
                        data.putString("oms",omschString);
                        omschrijving.setArguments(data);
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameDetail,omschrijving).commit();
                        break;
                    case 2:
                        data.putString("spec",specString);
                        specification.setArguments(data);
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameDetail,specification).commit();
                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }



    public void Doldur(JsonObject xml){
        git =new ArrayList<String>();
        resimlerJson= (JsonArray) xml.get("arrImages");


        for(int i=0;i<resimlerJson.size();i++){
            git.add(resimlerJson.get(i).getAsString());
        }

        mCostumPagerAdapter=new CostumPagerAdapter(getApplicationContext(), git);
        viewPager= (ViewPager) findViewById(R.id.viewDetail);
        viewPager.setAdapter(mCostumPagerAdapter);

        JsonArray specs= (JsonArray) xml.get("arrSpecs");
        specString=specs.toString();

        omschString=xml.get("strDescrLong").getAsString();
        infoString=xml.get("strDescrShort").getAsString();

        progressDialog.dismiss();

        informatieDetail infomatie=new informatieDetail();
        Bundle data=new Bundle();
        data.putInt("intProductOfferId",intProductOfferId);
        data.putString("inf",infoString);
        data.putInt("stock",intStock);
        data.putInt("kargo",kargo);
        data.putDouble("duoPrijs",prijs);
        infomatie.setArguments(data);
        getSupportFragmentManager().beginTransaction().replace(R.id.frameDetail, infomatie).commit();

    }


}

