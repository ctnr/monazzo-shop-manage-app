package com.munazzo.app.munazzo.Zoeken;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.munazzo.app.munazzo.Details.detailActivity;
import com.munazzo.app.munazzo.Login.sessionControl;
import com.munazzo.app.munazzo.MainActivity;
import com.munazzo.app.munazzo.MobilWebView;
import com.munazzo.app.munazzo.R;
import com.munazzo.app.munazzo.RetroClient;
import com.munazzo.app.munazzo.RetroInterface;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class movieAdapter extends RecyclerView.Adapter<movieAdapter.moviewViewHolder> {
    private List<pojoClass> movies;
    private int rowLayout;
    private Context context;
    ArrayList<Integer> products=new ArrayList(){};

    @Override
    public moviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(context).inflate(rowLayout, parent, false);
            return new moviewViewHolder(view);

    }

    @Override
    public void onBindViewHolder(moviewViewHolder holder, int position) {

    }



    @Override
    public int getItemCount() {
        return movies.size();
    }




    public static class moviewViewHolder extends RecyclerView.ViewHolder{

        CardView cardView;
        ImageView thumbnail;
        TextView title,price,stock,delivery;
        CheckBox checkBox;


        public moviewViewHolder(View itemView) {
            super(itemView);
            cardView=(CardView)itemView.findViewById(R.id.card_view);
            thumbnail=(ImageView)itemView.findViewById(R.id.image);
            title=(TextView)itemView.findViewById(R.id.list_item_Title);
            price=(TextView)itemView.findViewById(R.id.list_item_Prijs);
            stock=(TextView)itemView.findViewById(R.id.list_item_Stock);
            delivery=(TextView)itemView.findViewById(R.id.list_item_Dagen);
            checkBox=(CheckBox)itemView.findViewById(R.id.checkbox_item_list);

        }

    }

    public movieAdapter(List<pojoClass> movies, int rowLayout,Context context) {

        this.movies = new ArrayList<>(movies);
       // this.movies.addAll(movies);
        this.rowLayout = rowLayout;
        this.context = context;
    }

    public void setItem(List<pojoClass> persons){

        this.movies.addAll(persons);

    }

    @Override
    public void onBindViewHolder(final moviewViewHolder holder, final int position, List<Object> payloads) {

        holder.title.setText(movies.get(position).strTitle);
        holder.price.setText(new DecimalFormat("####.##").format(movies.get(position).douPrice));
        holder.stock.setText(String.valueOf( movies.get(position).intStock));
        holder.delivery.setText(String.valueOf(movies.get(position).intMaxDelivery));
        Glide.with(context).load(movies.get(position).strImageURL).
                thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .crossFade()
                .into(holder.thumbnail);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int ortalama=movies.get(position).intMaxDelivery+movies.get(position).intMinDelivery;
                ortalama=ortalama/2;
                if (new sessionControl(context).getSession()==true){

                    Intent dtail=new Intent(context.getApplicationContext(), detailActivity.class);
                    dtail.putExtra("intProductId",movies.get(position).intProductID);
                    dtail.putExtra("strTitle",movies.get(position).strTitle);
                    dtail.putExtra("intStock",movies.get(position).intStock);
                    dtail.putExtra("intProductOfferId",movies.get(position).intProductOfferID);
                    dtail.putExtra("duoPrijs",movies.get(position).douPrice);
                    dtail.putExtra("ortalama",ortalama);
                    context.startActivity(dtail);
                }
            }
        });

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    products.add(movies.get(position).intProductID);
                    holder.checkBox.setChecked(true);
                }else {
                    products.remove(new Integer(movies.get(position).intProductID));
                    holder.checkBox.setChecked(false);
                }

                if (products.isEmpty()){
                    MainActivity.fabVergelijk.hide();
                }else{
                    MainActivity.fabVergelijk.show();
                }
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                holder.checkBox.setChecked(holder.checkBox.isChecked()?false:true);

                return true;
            }
        });


        MainActivity.fabVergelijk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String toplam="";
                for(int i=0; i<products.size();i++){
                    if(i!=0){
                        toplam+="-";
                    }
                    toplam+=products.get(i).toString();
                }

                RetroInterface service = RetroClient.getClient(context).create(RetroInterface.class);
                Call<String> call=service.addToQueue(toplam);
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        Log.e("Send vergelijk list",response.body().toString());
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.e("Error: Vergelijk List",t.getMessage());
                    }
                });
                Intent vergelijk=new Intent(context, MobilWebView.class);
                vergelijk.putExtra("menu",R.id.product);
                context.startActivity(vergelijk);
            }

        });

        }

}



