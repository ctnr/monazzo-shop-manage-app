package com.munazzo.app.munazzo;

import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.munazzo.app.munazzo.Account.account;
import com.munazzo.app.munazzo.Bestellingen.bestellingen;
import com.munazzo.app.munazzo.Dashboard.dashboard;
import com.munazzo.app.munazzo.Fragments.webFragment;
import com.munazzo.app.munazzo.Login.login;
import com.munazzo.app.munazzo.Login.sessionControl;
import com.munazzo.app.munazzo.Products.product;
import com.munazzo.app.munazzo.Status.Status;
import com.munazzo.app.munazzo.winkelwagen.winkelwagen;

public class MobilWebView extends AppCompatActivity {

    FrameLayout mFrameLayout;
    sessionControl sessionControl;
    boolean sessionDurum;
    public Toolbar toolbar;
    public static TextView mobilTitle;
    NetworkChangeReceiver receiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobil_web_view);
        toolbar=(Toolbar)findViewById(R.id.toolbarMobil);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mobilTitle=(TextView)findViewById(R.id.MobileTitle);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_righ);
            }
        });


        IntentFilter filter=new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver=new NetworkChangeReceiver();
        registerReceiver(receiver,filter);


        mFrameLayout=(FrameLayout)findViewById(R.id.frame);

        sessionControl=new sessionControl(this);
        sessionDurum=sessionControl.getSession();
        int id=getIntent().getIntExtra("menu",0);

        pageControl(id);

    }

    public void pageControl(int id){


        if (id == R.id.dashboard) {
            if (sessionDurum) {
                toolbar.setTitle("Dashboard");
                getSupportFragmentManager().beginTransaction().replace(R.id.frame, new dashboard()).commit();

            }else{

                getSupportFragmentManager().beginTransaction().replace(R.id.frame,new login()).commit();
            }

        } else if (id == R.id.account) {
            if (sessionDurum) {
                getSupportFragmentManager().beginTransaction().replace(R.id.frame, new account()).commit();
            }else{
                getSupportFragmentManager().beginTransaction().replace(R.id.frame,new login()).commit();
            }

        } else if (id == R.id.bestellingen) {
            if (sessionDurum) {
                getSupportFragmentManager().beginTransaction().replace(R.id.frame, new bestellingen()).commit();
            }else{
                getSupportFragmentManager().beginTransaction().replace(R.id.frame,new login()).commit();
            }

        } else if (id == R.id.product) {
            if (sessionDurum) {

                getSupportFragmentManager().beginTransaction().replace(R.id.frame, new product()).commit();
            }else{

                getSupportFragmentManager().beginTransaction().replace(R.id.frame,new login()).commit();
            }

        } else if (id == R.id.import_stad) {
            if (sessionDurum) {

                getSupportFragmentManager().beginTransaction().replace(R.id.frame,new Status()).commit();
            }else{
                getSupportFragmentManager().beginTransaction().replace(R.id.frame,new login()).commit();
            }

        }else if(id==R.layout.winkelwagen) {
            if (sessionDurum) {
                getSupportFragmentManager().beginTransaction().replace(R.id.frame, new winkelwagen()).commit();
            } else {
                getSupportFragmentManager().beginTransaction().replace(R.id.frame, new login()).commit();
            }
        }else if (id==7){
            if (sessionDurum) {
                webFragment webFragment=new webFragment();
                Bundle arguman=new Bundle();
                arguman.putString("url",getIntent().getStringExtra("link"));
                webFragment.setArguments(arguman);
                getSupportFragmentManager().beginTransaction().replace(R.id.frame, webFragment).commit();
            } else {
                getSupportFragmentManager().beginTransaction().replace(R.id.frame, new login()).commit();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_righ);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
