package com.munazzo.app.munazzo.Dashboard;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.webkit.CookieManager;
import android.widget.Button;
import android.widget.TextView;

import com.munazzo.app.munazzo.Bestellingen.bestellingen;
import com.munazzo.app.munazzo.Login.sessionControl;
import com.munazzo.app.munazzo.MainActivity;
import com.munazzo.app.munazzo.MobilWebView;
import com.munazzo.app.munazzo.Products.product;
import com.munazzo.app.munazzo.R;
import com.munazzo.app.munazzo.RetroClient;
import com.munazzo.app.munazzo.RetroInterface;
import com.github.lzyzsd.circleprogress.ArcProgress;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 10.01.2017.
 */

public class dashboard extends Fragment implements View.OnClickListener{

    Button zoek, bestellingen, informaatie;

    ArcProgress progressZoek,progressbestell,progressMisluik,progressVerborgen,progressGeimporteerd;

    TextView textViewZoek,textViewBestell,textViewInformatie;

    RetroInterface service;

    Timer timer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.dashboard,container,false);

        sessionControl sessioncontrol=new sessionControl(getContext());
        CookieManager.getInstance().setCookie("https://munazzo.com",sessioncontrol.getCookie());


        MobilWebView.mobilTitle.setText("Dashboard");

        zoek=(Button)view.findViewById(R.id.button_zoek);
        bestellingen=(Button)view.findViewById(R.id.button_bestellingen);
        informaatie=(Button)view.findViewById(R.id.button_informatie);

        zoek.setOnClickListener(this);
        bestellingen.setOnClickListener(this);
        informaatie.setOnClickListener(this);

        progressZoek=(ArcProgress)view.findViewById(R.id.arc_progress_zoek);
        progressbestell=(ArcProgress)view.findViewById(R.id.arc_progress_bestellingen);
        progressMisluik=(ArcProgress)view.findViewById(R.id.arc_progress_informatie_misluik);
        progressVerborgen=(ArcProgress)view.findViewById(R.id.arc_progress_informatie_verborgen);
        progressGeimporteerd=(ArcProgress)view.findViewById(R.id.arc_progress_informatie_geimporteerd);

        textViewZoek=(TextView)view.findViewById(R.id.textView_zoek);
        textViewBestell=(TextView)view.findViewById(R.id.textView_bestellingen);
        textViewInformatie=(TextView)view.findViewById(R.id.textView_informatie);

        timer=new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {


        service= RetroClient.getClient(getContext()).create(RetroInterface.class);

        Call<JsonObject> calldashboard=service.getDasboard();
        calldashboard.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject object=response.body();
                int productCount=object.get("productcount").getAsInt();
                if (progressZoek.getProgress()!=productCount) {
                    ObjectAnimator anim = ObjectAnimator.ofInt(progressZoek, "progress", 0, productCount);
                    anim.setInterpolator(new DecelerateInterpolator());
                    anim.setDuration(500);
                    anim.start();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });

        Call<JsonArray>getBestellingen=service.getBestellingen();
        getBestellingen.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                JsonArray array=response.body();
                int bestell=array.size();
                progressbestell.setProgress(bestell);
                if (progressbestell.getProgress()!=bestell) {
                    ObjectAnimator anim = ObjectAnimator.ofInt(progressbestell, "progress", 0, bestell);
                    anim.setInterpolator(new DecelerateInterpolator());
                    anim.setDuration(500);
                    anim.start();
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {

            }
        });

        Call<JsonArray>getInformatie=service.getInformatie();
        getInformatie.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                JsonArray array=response.body();

                int updated=0,error=0,disabled=0;
                for (int i =0;i<array.size();i++){
                    JsonObject object=array.get(i).getAsJsonObject();
                    String strStatus=object.get("strStatus").getAsString();
                    if (strStatus.contains("UPDATED") || strStatus.contains("CREATED")){
                        updated++;
                    }else if (strStatus.contains("DISABLED")){
                        disabled++;
                    }else {
                        error++;
                    }
                }

            //    progressGeimporteerd.setProgress(updated);
            //    progressVerborgen.setProgress(disabled);
            //    progressMisluik.setProgress(error);

                if (progressMisluik.getProgress()!=error) {
                    ObjectAnimator anim = ObjectAnimator.ofInt(progressMisluik, "progress", 0, error);
                    anim.setInterpolator(new DecelerateInterpolator());
                    anim.setDuration(500);
                    anim.start();
                }

                if (progressGeimporteerd.getProgress()!=updated) {
                    ObjectAnimator anim = ObjectAnimator.ofInt(progressGeimporteerd, "progress", 0, updated);
                    anim.setInterpolator(new DecelerateInterpolator());
                    anim.setDuration(500);
                    anim.start();
                }

                if (progressVerborgen.getProgress()!=disabled) {

                    ObjectAnimator anim2 = ObjectAnimator.ofInt(progressVerborgen, "progress", 0, disabled);
                    anim2.setInterpolator(new DecelerateInterpolator());
                    anim2.setDuration(500);
                    anim2.start();
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {

            }
        });

            }
        },0,5000);
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (timer!=null){
            timer.cancel();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==zoek.getId()){
            Intent main=new Intent(getActivity(), MainActivity.class);
            MainActivity.tabs.getTabAt(0).select();
            startActivity(main);
        }else if (v.getId()==bestellingen.getId()){
            getFragmentManager().beginTransaction().replace(R.id.frame,new bestellingen()).commit();
        }else if (v.getId()==informaatie.getId()){
            getFragmentManager().beginTransaction().replace(R.id.frame,new product()).commit();
        }
    }
}
