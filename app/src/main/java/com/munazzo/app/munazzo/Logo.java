package com.munazzo.app.munazzo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.munazzo.app.munazzo.Login.sessionControl;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Logo extends AppCompatActivity {
    String username,password,cookie;
    sessionControl session;
    RetroInterface retroInterface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);
        session=new sessionControl(Logo.this);
        username=session.getUsername();
        password=session.getPassword();
        cookie=session.getCookie();

                retroInterface= RetroClient.getClient(getApplicationContext()).create(RetroInterface.class);
                Call<String> call=retroInterface.loginCheck();
                call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {

                    if (response.body().equals("0")){

                        Call<String> stringCall =retroInterface.loginPost(username,password);
                        stringCall.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                int gelenCevap= Integer.parseInt(response.body());
                                if (gelenCevap==1){
                                    String cookieNew=response.headers().get("Set-Cookie");
                                    session.setmPreferences(true,cookieNew,username,password);
                                }else {
                                    session.setmPreferences(false,"","name","password");
                                }

                                Intent i=new Intent(getApplicationContext(),MainActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();

                            }
                            @Override
                            public void onFailure(Call<String> call, Throwable t) {

                                Log.e("LogoError 2.kontrol",t.getMessage());

                            }
                        });

                    }else {
                        Intent i=new Intent(getApplicationContext(),MainActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                    }
                   // Log.e("session  ",session.getSession()+"");

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("LogoError IlkKontrol",t.getMessage());

                }
            });




    }


}
