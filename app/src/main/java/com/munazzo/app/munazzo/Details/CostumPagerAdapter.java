package com.munazzo.app.munazzo.Details;

import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.munazzo.app.munazzo.R;

import java.util.ArrayList;

/**
 * Created by admin on 13.02.2017.
 */

public class CostumPagerAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    detailActivity activity;
    ArrayList<String> listURL=new ArrayList<>();
    int [] resourch =new int[]{R.drawable.header,R.drawable.ic_account_24dp, R.drawable.munazzo_logo_wit};
    public CostumPagerAdapter(Context context, ArrayList<String> resimler) {
        mContext = context;
        listURL = resimler;
    }

    @Override
    public int getCount() {

        return listURL.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view==(LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = mLayoutInflater.inflate(R.layout.detail_image_pager,container,false);
        ImageView imageDetail=(ImageView)view.findViewById(R.id.imageDetail);
        Glide.with(mContext).load(Uri.parse(listURL.get(position))).
                thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageDetail);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}
