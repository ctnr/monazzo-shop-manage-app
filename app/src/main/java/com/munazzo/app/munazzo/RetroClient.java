package com.munazzo.app.munazzo;

import android.content.Context;

import com.munazzo.app.munazzo.Login.sessionControl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by admin on 23.01.2017.
 */

public class RetroClient {
    public static final String BASE_URL = "https://munazzo.com/";
    public static String cookie;
    public static Context context;

    RetroClient(){
        BusStation.getBus().register(this);
    }



    public static Retrofit getClient(Context context) {

        sessionControl sessionControl=new sessionControl(context);
        cookie=sessionControl.getCookie();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Cookie",cookie);

                Request request = requestBuilder.build();

                return chain.proceed(request);
            }
        });

            Retrofit retrofit = new Retrofit.Builder()
                    .client(httpClient.build())

                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BASE_URL)
                    .build();
        return retrofit;

    }

    public static Retrofit getStringClient(Context context){
        sessionControl sessionControl=new sessionControl(context);
        cookie=sessionControl.getCookie();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Cookie",cookie);

                Request request = requestBuilder.build();

                return chain.proceed(request);
            }
        });
        Gson gson=new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .build();

        return retrofit;
    }

    public static Retrofit saveSession(){
        OkHttpClient.Builder httpBuilder= new OkHttpClient.Builder();
        Retrofit retrofit = new Retrofit.Builder()
                .client(httpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build();

        return retrofit;
    }



}

