package com.munazzo.app.munazzo.Category;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.munazzo.app.munazzo.BusStation;
import com.munazzo.app.munazzo.Login.sessionControl;
import com.munazzo.app.munazzo.R;
import com.munazzo.app.munazzo.RetroClient;
import com.munazzo.app.munazzo.RetroInterface;
import com.munazzo.app.munazzo.Zoeken.movieAdapter;
import com.munazzo.app.munazzo.Zoeken.pojoClass;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 29.12.2016.
 */

public class categorieen extends android.support.v4.app.Fragment{
    int [] icons={
            R.drawable.ic_thuis,
            R.drawable.ic_thuis,
            R.drawable.ic_computer,
            R.drawable.ic_kleding,
            R.drawable.ic_sport,
            R.drawable.ic_endustrieel,
            R.drawable.ic_eten,
            R.drawable.ic_persoonelijk,
            R.drawable.ic_kantor,
            R.drawable.ic_baby,
            R.drawable.ic_motorfietsen,
            R.drawable.ic_entertainment,
            R.drawable.ic_dierverzorging
    };

    RecyclerView recyclerCategory;
    ArrayList<CategoryPOJO> datalistCategory;
    MyRecylerAdapter adapter,adapterSub;
    movieAdapter movAdapter;
    List<pojoClass> movData=new ArrayList<>();
    sessionControl session;

    int ProductCategory,IceCat;
    boolean subCats;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View viewCat=inflater.inflate(R.layout.categorieen,container,false);
        recyclerCategory=(RecyclerView)viewCat.findViewById(R.id.recycler_category);
        recyclerCategory.setHasFixedSize(true);
        recyclerCategory.setLayoutManager(new LinearLayoutManager(getContext()));

        datalistCategory=new ArrayList<>();

        session=new sessionControl(getContext());

        adapter=new MyRecylerAdapter(datalistCategory,R.layout.categorien_item,getContext());
        recyclerCategory.setAdapter(adapter);

        getcats(0,icons);

        return viewCat;

    }
    public void getcats(int iceCate, @Nullable final int[] dizi){


        RetroInterface service= RetroClient.getClient(getContext()).create(RetroInterface.class);
        Call<JsonArray> call =service.getCategories(iceCate);
        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                JsonArray jsonArray=response.body();
                datalistCategory.clear();

                for(int i=0;i<jsonArray.size();i++){
                    JsonObject object=jsonArray.get(i).getAsJsonObject();
                    CategoryPOJO yenidata=new CategoryPOJO();
                    yenidata.intProductCategoryID=object.get("intProductCategoryID").getAsInt();
                    yenidata.strName=object.get("strName").getAsString();
                    yenidata.intIceCatID=object.get("intIceCatID").getAsInt();
                    yenidata.intIceCatParentID=object.get("intIceCatParentID").getAsInt();
                    yenidata.strSubCats=object.get("strSubCats").isJsonNull();
                    yenidata.iconID=(dizi == null) ? 0 : dizi[i];

                    if(yenidata.intProductCategoryID != 302) {
                        datalistCategory.add(yenidata);
                    }
                }
                adapterSub=new MyRecylerAdapter(datalistCategory,R.layout.categorien_item,getContext());
                recyclerCategory.setAdapter(adapterSub);
            }
            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                Log.e("Error: Category ilk",t.getMessage());
            }
        });
    }

    public void ara(String kelime){
        RetroInterface retroInterface= RetroClient.getClient(getContext()).create(RetroInterface.class);
        Call<pojoClass[]> call = retroInterface.getJsonValues(kelime);
        call.enqueue(new Callback<pojoClass[]>() {

            @Override
            public void onResponse(Call<pojoClass[]> call, Response<pojoClass[]> response) {
                movData= Arrays.asList(response.body());
                movAdapter=new movieAdapter(movData,R.layout.item_list,getContext());
                recyclerCategory.setAdapter(movAdapter);

            }

            @Override
            public void onFailure(Call<pojoClass[]> call, Throwable t) {
                Log.e("Error:Category arama ", t.getMessage());
            }

        });


    }

    @Override
    public void onResume() {
        super.onResume();
        getcats(0,icons);
        BusStation.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusStation.getBus().unregister(this);
    }

    @Subscribe
    public void tiklanamyiYakala(categorieen cats){
        if (cats.subCats){

            ara(String.valueOf(cats.ProductCategory));

        }else{
            getcats(cats.IceCat,null);
        }
    }


}

