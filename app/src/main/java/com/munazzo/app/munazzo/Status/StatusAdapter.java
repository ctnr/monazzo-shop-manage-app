package com.munazzo.app.munazzo.Status;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.munazzo.app.munazzo.R;
import com.github.lzyzsd.circleprogress.ArcProgress;

import java.util.List;

/**
 * Created by admin on 20.03.2017.
 */

public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.importViewHolder> {
    private List<StatusPOJO> movies;
    private int rowLayout;
    private Context context;


    @Override
    public importViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new importViewHolder(view);
    }


    @Override
    public void onBindViewHolder(importViewHolder holder, int position) {

    }


    @Override
    public int getItemCount() {
        return movies.size();
    }

    public static class importViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView Title;
        ArcProgress arcProgress;
        ImageView logo;


        public importViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_status);
            Title = (TextView) itemView.findViewById(R.id.text_status);
            arcProgress = (ArcProgress) itemView.findViewById(R.id.arc_progress);
            logo = (ImageView) itemView.findViewById(R.id.image_status);


        }

    }

    public StatusAdapter(List<StatusPOJO> movies, int rowLayout, Context context) {
        this.movies = movies;
        this.rowLayout = rowLayout;
        this.context = context;
    }




    public void onBindViewHolder(final importViewHolder holder, final int position, List<Object> payloads) {

        holder.Title.setText(movies.get(position).appName);



        Glide.with(context).load(movies.get(position).imageURL).
                thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .crossFade()
                .into(holder.logo);

        if (holder.arcProgress.getProgress()<movies.get(position).procent) {
            ObjectAnimator anim = ObjectAnimator.ofInt(holder.arcProgress, "progress", 0, movies.get(position).procent);
            anim.setInterpolator(new DecelerateInterpolator());
            anim.setDuration(500);
            anim.start();
        }

    }
}