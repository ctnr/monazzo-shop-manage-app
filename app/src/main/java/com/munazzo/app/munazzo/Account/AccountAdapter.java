package com.munazzo.app.munazzo.Account;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.munazzo.app.munazzo.R;

import java.util.List;

/**
 * Created by admin on 6.04.2017.
 */

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.AccountViewHolder> {
    private List<horizontalMenuPOJO> movies;
    private int rowLayout;
    private Context context;

    @Override
    public AccountViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(rowLayout,parent,false);
        return new AccountViewHolder(view);
    }


    @Override
    public void onBindViewHolder(AccountViewHolder holder, int position) {

    }


    @Override
    public int getItemCount() {
        return movies.size();
    }

    public static class AccountViewHolder extends RecyclerView.ViewHolder {

        CardView card;
        TextView name;
        ImageView logo;



        public AccountViewHolder(View itemView) {
            super(itemView);
            card=(CardView)itemView.findViewById(R.id.card_account_app);
            name=(TextView)itemView.findViewById(R.id.text_account_app);
            logo=(ImageView)itemView.findViewById(R.id.image_account_app);

        }

    }
    public AccountAdapter(List<horizontalMenuPOJO> movies, int rowLayout, Context context) {
        this.movies = movies;
        this.rowLayout = rowLayout;
        this.context = context;
    }


    public void onBindViewHolder(final AccountViewHolder holder, final int position, List<Object> payloads) {

        holder.name.setText(movies.get(position).appname);
        Glide.with(context).load(movies.get(position).applogo).
                thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL).
                into(holder.logo);





    }

}

