package com.munazzo.app.munazzo;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by admin on 3.03.2017.
 */

public class MyJSON {

    public static void saveJson(Context context,String mJsonResponse,String fileName){
        try {
            FileWriter fileWriter=new FileWriter(context.getFilesDir().getPath()+"/"+fileName);
            fileWriter.write(mJsonResponse);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static String getJson(Context context,String fileName){
        File file=new File(context.getFilesDir().getPath()+"/"+fileName);
        try {
            FileInputStream is=new FileInputStream(file);
            int size=is.available();
            byte[] buffer=new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer);

        } catch (IOException e) {
            Log.e("TAG", "Error in Reading: " + e.getLocalizedMessage());
            return null;
        }

    }


}
