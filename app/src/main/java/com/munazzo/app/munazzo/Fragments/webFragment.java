package com.munazzo.app.munazzo.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.munazzo.app.munazzo.R;

/**
 * Created by admin on 27.03.2017.
 */
public class webFragment extends Fragment  {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.web_fragment,container,false);


        WebView webViewMobil=(WebView)view.findViewById(R.id.webMobil);
        webViewMobil.getSettings().setJavaScriptEnabled(true);



        String url=getArguments().getString("url");
        if (url==null&&url.equals("#")){
            webViewMobil.loadUrl("www.munazzo.com/assets/themes/default/img/header.png");
        }else {
            webViewMobil.loadUrl(url);
        }

        return view;
    }
}
