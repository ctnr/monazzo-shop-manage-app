package com.munazzo.app.munazzo.winkelwagen;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.munazzo.app.munazzo.R;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by admin on 24.02.2017.
 */

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.moviewViewHolder> {
    private List<CartListPOJO> movies;
    private int rowLayout;
    private Context context;



    @Override
    public moviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(rowLayout,parent,false);
        return new MyRecyclerViewAdapter.moviewViewHolder(view);
    }


    @Override
    public void onBindViewHolder(moviewViewHolder holder, int position) {

    }


    @Override
    public int getItemCount() {
        return movies.size();
    }

    public static class moviewViewHolder extends RecyclerView.ViewHolder{

        CardView cardView;
        TextView title,price,stuk;


        public moviewViewHolder(View itemView) {
            super(itemView);
            cardView=(CardView)itemView.findViewById(R.id.card_view);
            title=(TextView)itemView.findViewById(R.id.text_title_cart_list);
            price=(TextView)itemView.findViewById(R.id.text_prijs_cart_list);
            stuk=(TextView)itemView.findViewById(R.id.text_stuk_winkel_list);
        }

    }
    public MyRecyclerViewAdapter(List<CartListPOJO> movies, int rowLayout, Context context) {
        this.movies = movies;
        this.rowLayout = rowLayout;
        this.context = context;
    }


    public void onBindViewHolder(MyRecyclerViewAdapter.moviewViewHolder holder, int position, List<Object> payloads) {

        holder.title.setText(movies.get(position).strTitle);
        holder.price.setText("€ "+new DecimalFormat("####.##").format(movies.get(position).duoPrice));
        holder.stuk.setText("x "+String.valueOf( movies.get(position).intQuantity));
    }

}