package com.munazzo.app.munazzo.Fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.munazzo.app.munazzo.Login.sessionControl;
import com.munazzo.app.munazzo.MobilWebView;
import com.munazzo.app.munazzo.R;

/**
 * Created by admin on 27.02.2017.
 */

public class betalen extends android.support.v4.app.Fragment{
    WebView webViewBetalen;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.betalen,container,false);
        sessionControl sessioncontrol=new sessionControl(getContext());
        CookieManager.getInstance().setCookie("https://munazzo.com",sessioncontrol.getCookie());

        webViewBetalen = (WebView) view.findViewById(R.id.webview_betalen);

        MobilWebView.mobilTitle.setText("Betalen");

        WebSettings settings= webViewBetalen.getSettings();
        settings.setJavaScriptEnabled(true);
         webViewBetalen.setWebViewClient(new WebViewClient());
         webViewBetalen.setWebChromeClient(new WebChromeClient());

        if (Build.VERSION.SDK_INT >= 19) {
             webViewBetalen.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        else {
             webViewBetalen.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

         webViewBetalen.loadUrl("https://munazzo.com/app/checkout?stealthmode=1");

        return view;
    }
}
