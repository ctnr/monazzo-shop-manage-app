package com.munazzo.app.munazzo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;

import com.munazzo.app.munazzo.Apps.apps;
import com.munazzo.app.munazzo.Category.categorieen;
import com.munazzo.app.munazzo.Zoeken.zoeken;
import com.munazzo.app.munazzo.Login.sessionControl;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static TabLayout tabs;
    ViewPager viewPager;
    public static FloatingActionButton fabVergelijk;
    FloatingActionButton fab;
    InputMethodManager imm;
    DrawerLayout drawer;
    boolean sessionDurum;
    sessionControl session;
    RetroInterface servis;
    NetworkChangeReceiver receiver;
    IntentFilter filter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        filter=new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver=new NetworkChangeReceiver();
        registerReceiver(receiver,filter);




        session=new sessionControl(this);
        sessionDurum=session.getSession();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);





        FirebaseMessaging.getInstance().subscribeToTopic("test");
        String token=FirebaseInstanceId.getInstance().getToken();
        sendToToken(token);


        viewPager=(ViewPager)findViewById(R.id.viewPager);
        setupViewPager(viewPager);

        Intent service=new Intent(this,NotificationService.class);                                          //service i baslatiyoruz burada
        startService(service);

        getSupportFragmentManager().beginTransaction().addToBackStack("tag").add(new zoeken(),"").commit();


        tabs=(TabLayout)findViewById(R.id.Tab);
        tabs.setupWithViewPager(viewPager);
        int tab=getIntent().getIntExtra("tab",0);
        tabs.getTabAt(tab).select();


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        imm=(InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
                if (tab.getPosition()==0){
                    fab.show();
                }else{
                    fab.hide();
                    fabVergelijk.hide();
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        fabVergelijk=(FloatingActionButton)findViewById(R.id.fab_paylasim);
        fabVergelijk.hide();
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                Intent a=new Intent(MainActivity.this,MobilWebView.class);
                a.putExtra("menu",R.layout.winkelwagen);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                startActivity(a);
            }
        });


        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }
    ViewPagerAdapter adapter;
    private void setupViewPager(ViewPager viewPager) {
        adapter= new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new zoeken(),"Zoeken");
        adapter.addFrag(new categorieen(),"Categorieën");
        adapter.addFrag(new apps(),"App Store");
        viewPager.setAdapter(adapter);

    }

    private void sendToToken(String token) {
        // token'ı servise gönderme işlemlerini bu methodda yapmalısınız

        servis=RetroClient.getClient(getApplicationContext()).create(RetroInterface.class);
        Call<String> kaydet=servis.tokenRegister("android",token);
        kaydet.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.e("token send succes",response.body().toString());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e("Error send Token",t.getMessage());
            }
        });


    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Munazzo");
            builder.setMessage("Wilt u de App verlaten?");
            builder.setNegativeButton("Nee", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int id) {

                    //İptal butonuna basılınca yapılacaklar.Sadece kapanması isteniyorsa boş bırakılacak

                }
            });


            builder.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //Tamam butonuna basılınca yapılacaklar
                    finish();
                }
            });


            builder.show();

        }

    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id==R.id.uitloggen) {



            Call<String> uit=servis.uitLoggen();

            session.setmPreferences(false, "", "user", "pass");
            CookieManager cookieManager = CookieManager.getInstance();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cookieManager.removeAllCookies(null);

            } else {
                cookieManager.removeAllCookie();

            }
            finish();
        }else{

            Intent i=new Intent(this,MobilWebView.class);
            i.putExtra("menu",id);
            startActivity(i);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
