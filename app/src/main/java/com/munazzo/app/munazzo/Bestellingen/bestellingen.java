package com.munazzo.app.munazzo.Bestellingen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;

import com.munazzo.app.munazzo.Login.sessionControl;
import com.munazzo.app.munazzo.MobilWebView;
import com.munazzo.app.munazzo.R;
import com.munazzo.app.munazzo.RetroClient;
import com.munazzo.app.munazzo.RetroInterface;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 10.01.2017.
 */

public class bestellingen extends Fragment{

    RecyclerView recyclerView;
    List<bestellingenPOJO> dataList;
    bestellingenAdapter adapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.bestellingen,container,false);

        sessionControl sessioncontrol=new sessionControl(getContext());
        CookieManager.getInstance().setCookie("https://munazzo.com",sessioncontrol.getCookie());

        MobilWebView.mobilTitle.setText("Bestellingen");

        recyclerView=(RecyclerView)view.findViewById(R.id.recycler_bestellingen);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        dataList=new ArrayList<>();

        RetroInterface service= RetroClient.getClient(getContext()).create(RetroInterface.class);
        Call<JsonArray> getBestellingen=service.getBestellingen();
        getBestellingen.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                JsonArray array=response.body();

                for (int i =0; i<array.size();i++){
                    bestellingenPOJO data=new bestellingenPOJO();

                    JsonObject object=array.get(i).getAsJsonObject();

                    String date=object.get("datPlatformOrderDate").getAsString();
                    data.datum=date.substring(0,10);
                    data.betaalen=object.get("intPayed").getAsInt();
                    data.verzonden=object.get("intShipped").getAsInt();
                    data.prijs=object.get("douTotal").getAsDouble();
                    data.winkelID=object.get("strPlatformReference").getAsInt();
                    data.status=object.get("intStatusID").getAsInt();
                    JsonObject platformObject=object.get("objPlatform").getAsJsonObject();
                    data.winkel=platformObject.get("strName").getAsString();

                    dataList.add(data);
                }

                adapter=new bestellingenAdapter(dataList,R.layout.bestellingen_item,getContext());
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                Log.e("Error: Bestellingen",t.getMessage());
            }
        });
        return view;
    }
}
