package com.munazzo.app.munazzo.Category;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.munazzo.app.munazzo.BusStation;
import com.munazzo.app.munazzo.R;

import java.util.List;

/**
 * Created by admin on 28.02.2017.
 */

public class MyRecylerAdapter extends RecyclerView.Adapter<MyRecylerAdapter.moviewViewHolder> {
    private List<CategoryPOJO> movies;
    private int rowLayout;
    private Context context;




    @Override
    public MyRecylerAdapter.moviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(rowLayout,parent,false);
        return new MyRecylerAdapter.moviewViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MyRecylerAdapter.moviewViewHolder holder, int position) {

    }


    @Override
    public int getItemCount() {
        return movies.size();
    }

    public static class moviewViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView categoryTitle;
        ImageView categoryIcon, categorySub;


        public moviewViewHolder(View itemView) {
            super(itemView);
            cardView=(CardView)itemView.findViewById(R.id.card_category);
            categoryTitle=(TextView)itemView.findViewById(R.id.text_category);
            categoryIcon=(ImageView)itemView.findViewById(R.id.image_category_icon);
            categorySub=(ImageView)itemView.findViewById(R.id.image_alt_category);

        }

    }
    public MyRecylerAdapter(List<CategoryPOJO> movies, int rowLayout, Context context) {
        this.movies = movies;
        this.rowLayout = rowLayout;
        this.context = context;
    }


    public void onBindViewHolder(final MyRecylerAdapter.moviewViewHolder holder, final int position, final List<Object> payloads) {

        holder.categoryTitle.setText(movies.get(position).strName);
        holder.categoryIcon.setImageResource(movies.get(position).iconID);
        if (!movies.get(position).strSubCats){
            holder.categorySub.setImageResource(R.drawable.ic_subcategory);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categorieen cats=new categorieen();
                cats.IceCat=movies.get(position).intIceCatID;
                cats.ProductCategory=movies.get(position).intProductCategoryID;
                cats.subCats=movies.get(position).strSubCats;
                BusStation.getBus().post(cats);
            }
        });


    }

}
