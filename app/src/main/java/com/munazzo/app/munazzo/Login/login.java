package com.munazzo.app.munazzo.Login;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.munazzo.app.munazzo.Dashboard.dashboard;
import com.munazzo.app.munazzo.R;
import com.munazzo.app.munazzo.RetroClient;
import com.munazzo.app.munazzo.RetroInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class login extends Fragment {

    public String username,password,cookie;
    int gelenCevap;
    EditText user,pass;
    Button login, newAccount;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        final sessionControl sessionControl=new sessionControl(getActivity().getApplicationContext());
        View loginView=inflater.inflate(R.layout.fragment_login, container, false);
        user=(EditText) loginView.findViewById(R.id.username);
        pass=(EditText) loginView.findViewById(R.id.password);
        login=(Button) loginView.findViewById(R.id.btnLogin);


        user.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(user, InputMethodManager.SHOW_IMPLICIT);


        newAccount=(Button) loginView.findViewById(R.id.btnYeniKayit);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username=user.getText().toString();
                password=pass.getText().toString();
                RetroInterface retroInterface=RetroClient.saveSession().create(RetroInterface.class);
                Call<String> call=retroInterface.loginPost(username,password);
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        gelenCevap= Integer.parseInt(response.body());
                        

                        if (gelenCevap==1) {
                            String cook=response.headers().get("Set-Cookie");
                            sessionControl.setmPreferences(true,cook,username,password);
                            getFragmentManager().beginTransaction().replace(R.id.frame,new dashboard()).commit();
                        } else {
                            user.setError("Email of Password niet waar");
                            pass.setError("Email of Password niet waar");
                        }
                    
                    
                    
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.e("Error: Login ",t.getMessage());
                    }
                });

            }
        });

        newAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getFragmentManager().beginTransaction().replace(R.id.frame,new newAccount()).commit();
            }
        });

        return loginView;
    }

}
