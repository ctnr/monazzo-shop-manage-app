package com.munazzo.app.munazzo;

import com.munazzo.app.munazzo.Zoeken.pojoClass;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by admin on 24.01.2017.
 */

public interface RetroInterface {
    @GET("api/search")
    Call<pojoClass[]> getJsonValues(@Query("term") String order);

    @FormUrlEncoded
    @POST("api/authenticate")
    Call<String> loginPost(

            @Field("username") String username,
            @Field("password") String password
    );

    @GET("api/authenticated")
    Call<String> loginCheck();

    @GET("app/logout")
    Call<String> uitLoggen();

    @GET("api/detail")
    Call<JsonObject> getDetail(@Query("id") int intproductId);

    @GET("api/addtocart")
    Call<String> addtocart(
            @Query("offer") int offer,
            @Query("quantity") int quantity
    );
    @GET("api/cart")
    Call<JsonObject> getcart();

    @GET("api/deletefromcart")
    Call<String> delete(@Query("cart") int intCartID);

    @GET("api/editcart")
    Call<String> editCartItem(
            @Query("cart") int intCartID,
            @Query("quantity") int intQuantity
    );

    @GET("api/categories")
    Call<JsonArray> getCategories(
            @Query("parent") int intIceCatID
    );


    @GET("api/store")
    Call<JsonArray> getApps();

    @GET("api/search")
    Call<pojoClass[]> getJsonValuePage(
            @Query("term") String kelime,
             @Query("page") int sayfa
    );

    @GET("api/addtoqueue")
    Call<String> addToQueue(
            @Query("products") String products
    );

    @GET("api/queue")
    Call<JsonArray> getQueue();

    @GET("api/platforms")
    Call<JsonArray> getPlatforms();

    @GET("api/platformcategories")
    Call<JsonArray> getPlatformCategories(@Query("index") int index );

    @GET("api/deletefromqueue")
    Call<String> deleteFromQueue(@Query("intProductQueueID") int productQueueID);


    @FormUrlEncoded
    @POST("api/addproduct")
    Call<String> addProductQueue(
            @Field("index") int index,
            @Field("app") int app,
            @Field("products") String products
    );

    @GET("api/importlog")
    Call<JsonArray>importLog();

    @GET("api/startimport")
    Call<String>startImport(@Query("index") int index);

    @GET("/api/dashboard")
    Call<JsonObject> getDasboard();

    @GET("/api/orders")
    Call<JsonArray> getBestellingen();

    @GET("/api/importqueue")
    Call<JsonArray> getInformatie();

    @GET("/api/registerdevice")
    Call<String> tokenRegister(
            @Query("platform") String platform,              //https://munazzo/api/registerdevice?platform=android&token=
            @Query("token")String token
    );
    @GET("/api/getaccount")
    Call<JsonObject> getAccount();
}



