package com.munazzo.app.munazzo.Products;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.munazzo.app.munazzo.BusStation;
import com.munazzo.app.munazzo.Fragments.addNewApp;
import com.munazzo.app.munazzo.MobilWebView;
import com.munazzo.app.munazzo.R;
import com.munazzo.app.munazzo.RetroClient;
import com.munazzo.app.munazzo.RetroInterface;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 10.01.2017.
 */

public class product extends Fragment implements AdapterView.OnItemSelectedListener{

    Spinner spinnerPlatform;
    ArrayList<String> spinnerList,spinnerListMarkt,spinnerIcon;
    ArrayAdapter spinnerAdapter;
    RecyclerView recyclerProduct;
    ProductRecyclerAdapter adapter;
    ArrayList<Integer>ID;
    ArrayList<Integer>IDapp;

    TextView rakam;
    public static Button importeren;
    RetroInterface service;
    ArrayList<ProductPOJO> productData;

    AlertDialog progress;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.product,container,false);


        progress=new SpotsDialog(getContext(),R.style.CustomMunazzo);

        spinnerPlatform=(Spinner)view.findViewById(R.id.product_spinner);
        spinnerList =new ArrayList<>();
        spinnerListMarkt=new ArrayList<>();
        spinnerIcon=new ArrayList<>();

        MobilWebView.mobilTitle.setText("Product Import");

        recyclerProduct= (RecyclerView) view.findViewById(R.id.recycler_product);
        recyclerProduct.setHasFixedSize(true);
        recyclerProduct.setLayoutManager(new LinearLayoutManager(getContext()));


        rakam=(TextView)view.findViewById(R.id.text_product_importeren);
        importeren=(Button)view.findViewById(R.id.button_product_importeren);

        IDapp=new ArrayList<>();



        getApps();


        spinnerPlatform.setOnItemSelectedListener(this);

        ItemTouchHelper.SimpleCallback simpleCallback=new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                deleteQueue(productData.get(viewHolder.getAdapterPosition()).ProductQueueID);
                getImport(0);

            }

        };

        ItemTouchHelper iteItemTouchHelper=new ItemTouchHelper(simpleCallback);
        iteItemTouchHelper.attachToRecyclerView(recyclerProduct);

        return view;
    }

    public void getApps(){
        service= RetroClient.getClient(getActivity()).create(RetroInterface.class);
        Call<JsonArray> platforms=service.getPlatforms();
        platforms.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                JsonArray jsonArray=response.body();

                    for (int sayi = 0; sayi < jsonArray.size(); sayi++) {
                        JsonObject object = jsonArray.get(sayi).getAsJsonObject();
                        JsonObject objectAlt = object.get("arrProperties").getAsJsonObject();
                        JsonObject objectAltinAlti = objectAlt.get("objPlatform").getAsJsonObject();
                        JsonObject objectEnAlt = objectAltinAlti.get("arrProperties").getAsJsonObject();
                        String category = objectEnAlt.get("strCategory").getAsString();
                        if (category.equals("Webshop Integratie")) {
                            spinnerList.add(objectEnAlt.get("strTitle").getAsString());
                            IDapp.add(1);
                        } else if (category.equals("Marktplaats")) {
                            spinnerList.add(objectEnAlt.get("strTitle").getAsString());
                            IDapp.add(0);
                        }
                    }
                    spinnerAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, spinnerList);
                    spinnerPlatform.setAdapter(spinnerAdapter);
                    if(spinnerList.size()==0){
                        getFragmentManager().beginTransaction().replace(R.id.frame,new addNewApp()).commit();
                    }

            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                Log.e("Error:Product",t.getMessage());
            }
        });
    }


    public void deleteQueue(int productqueueID){
        Call<String> delete=service.deleteFromQueue(productqueueID);
        delete.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                getImport(0);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getContext(), "hata olustu", Toast.LENGTH_SHORT).show();
            }
        });
        
    }



    public void getImport(final int position){
        Call<JsonArray> call=service.getQueue();
        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

                productData=new ArrayList<>();
                JsonArray responArray= response.body().getAsJsonArray();
                ArrayList<String>categoriler= categoryList(position);

                    for (int i = 0; i < responArray.size(); i++) {
                        ProductPOJO data = new ProductPOJO();
                        JsonObject element = responArray.get(i).getAsJsonObject();
                        data.Title = element.get("strTitle").getAsString();
                        data.Prijs = element.get("douPrice").getAsDouble();
                        data.ProductID = element.get("intProductID").getAsInt();
                        data.ProductQueueID = element.get("intProductQueueID").getAsInt();
                        data.verkoop = (float) ((data.Prijs * 0.3) + data.Prijs);
                        data.categorys = categoriler;
                        data.categoryID = ID;
                        data.appindex = spinnerPlatform.getSelectedItemPosition();
                        productData.add(data);

                    }
                    rakam.setText(String.valueOf(productData.size()));
                    adapter = new ProductRecyclerAdapter(productData, R.layout.product_item, getActivity());
                    recyclerProduct.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                Log.e("Error:Product",t.getMessage());
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        getImport(position);
        }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    public ArrayList<String> categoryList(int position){

        progress.show();

        ID=new ArrayList<>();
        final ArrayList<String> list=new ArrayList<>();
        if(IDapp.get(spinnerPlatform.getSelectedItemPosition())==0){
            list.add("Geen categorie..");
        }else {
            list.add("Kies uw categorie...");
        }
        ID.clear();
        ID.add(0);
        Call<JsonArray> getCategory = service.getPlatformCategories(position);
        getCategory.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {



                JsonArray array = response.body();
               if (0<array.size()) {


                   for (int i = 0; i < array.size(); i++) {
                       JsonObject obje = array.get(i).getAsJsonObject();
                       list.add(obje.get("name").getAsString());
                       ID.add(obje.get("id").getAsInt());
                   }
               }

            progress.dismiss();
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                Log.e("Error: Product 3",t.getMessage());
            }
        });

        return list;
    }

    @Override
    public void onResume() {
        super.onResume();
        BusStation.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusStation.getBus().unregister(this);
    }
    @Subscribe
    public void setProductQueue(Messenger m){

        final int spinnerPosition=spinnerPlatform.getSelectedItemPosition();
        Toast.makeText(getContext(), ""+m.gelen+"\n "+spinnerPosition+"\n"+IDapp.get(spinnerPosition), Toast.LENGTH_SHORT).show();


        Call<String> startQueue=service.addProductQueue(0,1,m.gelen);
        startQueue.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                getImport(spinnerPosition);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e("Error: product addqueue",t.getMessage());
            }
        });

        Call<String> start=service.startImport(spinnerPlatform.getSelectedItemPosition());
        start.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                Log.e("start cevap: ",response.body().toString());

                Intent imp =new Intent(getActivity(),MobilWebView.class);
                imp.putExtra("menu",R.id.import_stad);
                startActivity(imp);

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e("Error:Product Bus ",t.getMessage());
            }
        });

    }







}
