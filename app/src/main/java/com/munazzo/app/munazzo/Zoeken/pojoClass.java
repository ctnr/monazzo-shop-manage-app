package com.munazzo.app.munazzo.Zoeken;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 23.01.2017.
 */

public class pojoClass {

    String type;
    pojoClass(String type){
        this.type=type;
    }

    @SerializedName("intProductIndexID")
    @Expose
    public int intProductIndexID;
    @SerializedName("intProductID")
    @Expose
    public int intProductID;
    @SerializedName("intProductOfferID")
    @Expose
    public int intProductOfferID;
    @SerializedName("intProductInformationID")
    @Expose
    public int intProductInformationID;
    @SerializedName("intProductCategoryID")
    @Expose
    public int intProductCategoryID;
    @SerializedName("intProductBrandID")
    @Expose
    public int intProductBrandID;
    @SerializedName("intMinDelivery")
    @Expose
    public int intMinDelivery;
    @SerializedName("intMaxDelivery")
    @Expose
    public int intMaxDelivery;
    @SerializedName("intStock")
    @Expose
    public int intStock;
    @SerializedName("douPrice")
    @Expose
    public double douPrice;
    @SerializedName("strTitle")
    @Expose
    public String strTitle;
    @SerializedName("strDescription")
    @Expose
    public String strDescription;
    @SerializedName("strIdentities")
    @Expose
    public String strIdentities;
    @SerializedName("strImageURL")
    @Expose
    public String strImageURL;
    @SerializedName("strProductBrand")
    @Expose
    public String strProductBrand;
    @SerializedName("strProductCategory")
    @Expose
    public String strProductCategory;
    @SerializedName("strDelivery")
    @Expose
    public String strDelivery;
    @SerializedName("strCategories")
    @Expose
    public String strCategories;
    @SerializedName("intScore")
    @Expose
    public String intScore;
    @SerializedName("intOfferCount")
    @Expose
    public String intOfferCount;
    @SerializedName("douOldPrice")
    @Expose
    public String douOldPrice;

}

