package com.munazzo.app.munazzo.Apps;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.munazzo.app.munazzo.MyJSON;
import com.munazzo.app.munazzo.R;
import com.munazzo.app.munazzo.RetroClient;
import com.munazzo.app.munazzo.RetroInterface;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 29.12.2016.
 */

public class apps extends android.support.v4.app.Fragment{
    RecyclerView recyclerApps;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewApps=inflater.inflate(R.layout.apps,container,false);



        recyclerApps=(RecyclerView)viewApps.findViewById(R.id.recycler_apps);
        recyclerApps.setHasFixedSize(true);
        recyclerApps.setLayoutManager(new GridLayoutManager(getContext(),2));

        if (MyJSON.getJson(getContext(),"apps")==null) {

            RetroInterface service = RetroClient.getClient(getContext()).create(RetroInterface.class);
            Call<JsonArray> call = service.getApps();
            call.enqueue(new Callback<JsonArray>() {
                @Override
                public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                    JsonArray jsonArray=response.body();
                    parseJson(jsonArray);
                    String jsongonder=jsonArray.toString();
                    MyJSON.saveJson(getContext(),jsongonder,"apps");
                }

                @Override
                public void onFailure(Call<JsonArray> call, Throwable t) {
                    Log.e("Error: Apps Request",t.getMessage());
                }
            });
        } else {

           String json= MyJSON.getJson(getContext(),"apps");

            JsonParser parser=new JsonParser();
            JsonArray array= (JsonArray) parser.parse(json);
            parseJson(array);
            }

        return viewApps;
    }
    public void parseJson(JsonArray jsonArray){
        ArrayList<AppsPOJO> dataListApps = new ArrayList<AppsPOJO>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject object = jsonArray.get(i).getAsJsonObject();
            AppsPOJO appsData = new AppsPOJO();
            appsData.strName = object.get("strName").getAsString();
            appsData.strTite = object.get("strTitle").getAsString();
            appsData.strDescreption = object.get("strDescription").getAsString();
            appsData.strCategory = object.get("strCategory").getAsString();
            appsData.strIcon = "https://munazzo.com" + object.get("strIcon").getAsString();
            appsData.strPage = object.get("strPage").getAsString();
            appsData.strLink = object.get("strLink").getAsString();
            dataListApps.add(appsData);
        }
        AppsRecyclerAdapter adapter = new AppsRecyclerAdapter(dataListApps, R.layout.apps_item, getContext());
        recyclerApps.setAdapter(adapter);
    }

}
