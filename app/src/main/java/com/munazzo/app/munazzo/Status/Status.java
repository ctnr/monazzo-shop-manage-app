package com.munazzo.app.munazzo.Status;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.munazzo.app.munazzo.Fragments.addNewApp;
import com.munazzo.app.munazzo.MobilWebView;
import com.munazzo.app.munazzo.R;
import com.munazzo.app.munazzo.RetroClient;
import com.munazzo.app.munazzo.RetroInterface;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 21.03.2017.
 */

public class Status extends android.support.v4.app.Fragment {
    RetroInterface service;
    List<String>appName;
    List<String>iconUrl;
    Timer timer;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.status_import,container,false);
        final RecyclerView recyclerStatus=(RecyclerView)view.findViewById(R.id.recycler_status);
        recyclerStatus.setHasFixedSize(true);
        recyclerStatus.setLayoutManager(new LinearLayoutManager(getContext()));
        final List<StatusPOJO>dataList=new ArrayList<>();

        MobilWebView.mobilTitle.setText("Status Import");



        service= RetroClient.getClient(getActivity()).create(RetroInterface.class);
        Call<JsonArray> platforms=service.getPlatforms();
        platforms.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                JsonArray jsonArray=response.body();
                appName=new ArrayList<>();
                iconUrl=new ArrayList<>();
                for(int sayi=0;sayi<jsonArray.size();sayi++){
                    JsonObject object=jsonArray.get(sayi).getAsJsonObject();
                    JsonObject objectAlt=object.get("arrProperties").getAsJsonObject();
                    JsonObject objectAltinAlti=objectAlt.get("objPlatform").getAsJsonObject();
                    JsonObject objectEnAlt=objectAltinAlti.get("arrProperties").getAsJsonObject();


                        appName.add(objectEnAlt.get("strTitle").getAsString());
                        iconUrl.add(objectEnAlt.get("strIcon").getAsString());

                }
                if (appName.size()==0){
                    getFragmentManager().beginTransaction().replace(R.id.frame,new addNewApp()).commit();
                }else {
                    service = RetroClient.getClient(getContext()).create(RetroInterface.class);
                    Call<JsonArray> getir = service.importLog();
                    getir.enqueue(new Callback<JsonArray>() {
                        @Override
                        public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                            JsonArray array = response.body();
                            for (int i = 0; i < array.size(); i++) {
                                JsonObject object = array.get(i).getAsJsonObject();
                                StatusPOJO data = new StatusPOJO();

                                data.running = object.get("intScriptRunning").getAsInt();

                                double itemLeft = object.get("intItemsLeft").getAsDouble();
                                double update = object.get("intUpdateCount").getAsDouble();
                                double create = object.get("intCreateCount").getAsDouble();

                                double sayi3 = 0;
                                try {

                                    sayi3 = (data.running == 1) ? ((create + update) - itemLeft) / (create + update) * 100.0 : 100.0;
                                } catch (ArithmeticException e) {
                                    Log.e("matematikselHata", e.getMessage());

                                }

                                data.appName = appName.get(i);
                                data.imageURL = "https://munazzo.com" + iconUrl.get(i);
                                data.procent = (int) sayi3;

                                dataList.add(data);
                            }
                            StatusAdapter adapter = new StatusAdapter(dataList, R.layout.status_item, getContext());
                            recyclerStatus.setAdapter(adapter);
                            live(adapter, dataList);
                        }

                        @Override
                        public void onFailure(Call<JsonArray> call, Throwable t) {
                            Log.e("Error:Status iki", t.getMessage());
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                Log.e("Error: Status ilk ",t.getMessage());
            }
        });



        return view;
    }

    public void live(final StatusAdapter adapter, final List<StatusPOJO> dataList){

        timer=new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                service= RetroClient.getClient(getContext()).create(RetroInterface.class);
                Call<JsonArray> getir=service.importLog();
                getir.enqueue(new Callback<JsonArray>() {
                    @Override
                    public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                        JsonArray array=response.body();
                        for (int i=0;i<array.size();i++){
                            JsonObject object=array.get(i).getAsJsonObject();
                            StatusPOJO data=new StatusPOJO();

                            data.running=object.get("intScriptRunning").getAsInt();

                            double itemLeft=object.get("intItemsLeft").getAsDouble();
                            double update=object.get("intUpdateCount").getAsDouble();
                            double create=object.get("intCreateCount").getAsDouble();

                            double sayi3=0;
                            try {

                                sayi3 = (data.running==1) ? ((create + update) - itemLeft) / (create + update) * 100.0 : 100.0;
                            } catch(ArithmeticException e){
                                Log.e("matematikselHata",e.getMessage());

                            }


                            dataList.get(i).procent=(int)sayi3;
                            //data.appName=appName.get(i);
                            //data.imageURL="https://munazzo.com"+iconUrl.get(i);
                            //data.procent= (int) sayi3;

                            //dataList.add(data);
                        }

                        adapter.notifyDataSetChanged();


                    }

                    @Override
                    public void onFailure(Call<JsonArray> call, Throwable t) {
                        Log.e("Error: Status Live",t.getMessage());
                    }
                });
            }
        },0,5000);
    }


    @Override
    public void onStop() {
        super.onStop();
        if (timer!=null){
            timer.cancel();
        }
    }
}
